#! /usr/bin/python
'''Convert SAMI subtitles to SRT'''

import sys
import re
from os.path import exists
from argparse import ArgumentParser
from io import TextIOWrapper


r = {
    'nbsp': re.compile('&nbsp(;|$)'),
    'body': re.compile('<body>', re.I),
    'sync': re.compile('<sync(?P<attrs>[^>]*)>', re.I),
    'time': re.compile('start=(?P<ms>[0-9]+)', re.I),
    'br': re.compile('<br[^>]*>', re.I),
    'html': re.compile('<[^>]*>'),
}


def ms2time(ms):
    '''Convert a time in milliseconds to HH:MM:SS,mmm'''
    seconds, ms = divmod(ms, 1000)
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return f'{hours:02d}:{minutes:02d}:{seconds:02d},{ms:03d}'


def handle_line(line, linenum):
    '''Extract information from one line.'''

    text = ''
    time = None
    remaining = ''

    match = r['sync'].search(line)
    if match:
        if match.start() > 0:
            text = line[:match.start()]

        match2 = r['time'].search(match.group('attrs'))
        if match2:
            time = ms2time(int(match2.group('ms'))) if match2 else None
        else:
            print(f'{linenum} : {match.group(0)} : '
                  f'Malformed sync tag; ignoring')

        remaining = line[match.end():]
    else:
        # since there is no sync tag in the line, consider it all to be subitle
        # text
        text = line

    if text:
        text = r['br'].sub('\n', text)
        text = r['html'].sub('', text)

    return text, time, remaining

def convert_file(file_in, file_out):
    '''Convert SAMI subtitles to SRT'''
    body = False
    start = stop = None
    text = ''
    linenum = 0
    subnum = 1

    for line in file_in:
        linenum += 1
        line = r['nbsp'].sub(' ', line)
        line = line.strip().replace('\r', '')

        if not line:
            continue

        # ignore header until we find the <body> tag
        if not body and r['sync'].search(line):
            print('body tag seems to be missing', file=sys.stderr)
            body = True

        if not body:
            if r['body'].match(line):
                body = True
            else:
                continue

        # handle subtitle blocks
        while line:
            new_text, stop, line = handle_line(line, linenum)

            # note that if there is text preceding the sync tag, we must append
            # it before closing the subtitle below
            if new_text:
                text += new_text

            if stop is not None:
                if start and text.strip():
                    if stop:
                        print(f'{subnum}\n{start} --> {stop}\n{text.strip()}\n',
                              file=file_out)
                        subnum += 1
                    else:
                        print(f'{linenum}: Malformed subtitle, skipping',
                              file=sys.stderr)
                # start next subtitle
                start = stop
                stop = None
                text = ''


def main(args):
    '''Convert SAMI subtitles to SRT'''
    encoding = args.encoding
    errors = 'replace' if encoding else None

    if args.file:
        for file_in in args.file:
            file_out = file_in.replace('.smi', '')+'.srt'
            if exists(file_out) and not args.force:
                sys.exit(f'File exists: {file_out}')

            with open(file_in, encoding=encoding, errors=errors) as f_in:
                with open(file_out, 'w') as f_out:
                    convert_file(f_in, f_out)
    else:
        file_in = (
            TextIOWrapper(sys.stdin.buffer, encoding=encoding, errors=errors)
            if encoding else sys.stdin
        )
        convert_file(file_in, sys.stdout)


if __name__ == '__main__':
    _parser = ArgumentParser(description=__doc__)

    _parser.add_argument(
        'file', nargs='*',
        help='SAMI file to be converted (default: read from stdin)'
    )

    _parser.add_argument(
        '-f', '--force', action='store_true',
        help='Overwrite existing files'
    )

    _parser.add_argument(
        '-e', '--encoding',
        help='Input file encoding'
    )

    _args = _parser.parse_args()
    main(_args)
