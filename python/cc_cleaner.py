#! /usr/bin/env python3
'''Clean up closed captions extracted from MPEG-TS streams with ccextractor.'''

import sys
import argparse
import re
from functools import total_ordering
from itertools import chain


ext_regex = re.compile(r'\.srt$', re.I)
sub_num_regex = re.compile(r'^\d+\s*$')


@total_ordering
class Subtitle:
    '''One subtitle read from a SRT file'''

    _time_str = r'\d{2}:\d{2}:\d{2},\d{3}'
    _time_line_regex = re.compile(
        r'^(?P<start>{t}) --> (?P<end>{t})\s*$'.format(t=_time_str))

    start = None
    end = None
    lines = None

    # only used for sorting subs with equal times
    original_number = 0

    def __init__(self, original_number=None):
        self.lines = []

        if original_number:
            self.original_number = original_number

    def read_times(self, line):
        '''Read start and end time from a line of text.'''
        match = self._time_line_regex.match(line)
        if not match:
            raise ValueError(f'Invalid time line: {line}')

        self.start = match.group('start')
        self.end = match.group('end')

    def append_line(self, line):
        '''Append a line of text to the subtitle.

        Returns the subtitle length, or None if the line was empty (i.e. the
        subtitle is over)
        '''
        line = line.strip()

        if line:
            self.lines.append(line)
            return len(self.lines)
        return None

    def join_lines(self):
        '''Join all current lines, using a space as separator.'''
        self.lines = [' '.join(self.lines)]

    def __str__(self):
        if not (self.start and self.end):
            raise ValueError('Start and end times are missing')
        if not self.lines:
            raise ValueError('The subtitle is empty')

        # NOTE that the caller is responsible for printing the correct subtitle
        # number. The original number is used for sorting only and will not be
        # output.
        lines = [f'{self.start} --> {self.end}']
        lines += self.lines

        # NOTE that we end the subtitle with a final line break, but the caller
        # should still separate two subsequent subtitle blocks with yet another
        # line break
        return '\n'.join(lines) + '\n'

    def _sortable_str(self):
        '''Generate string representation for sorting subs.'''
        return ':'.join([self.start, self.end, str(self.original_number or 0),
                         '\n'.join(self.lines)])

    def __lt__(self, other):
        return self._sortable_str() < other._sortable_str()

    # NOTE that subs that are exactly the same except for their original numbers
    # will be considered unequal. To properly compare disregarding the original
    # number, use self.equals(other)
    def __eq__(self, other):
        return self._sortable_str() == other._sortable_str()

    @staticmethod
    def union(subs):
        '''Return union of a list of subtitles.'''

        if not subs:
            # nothing to unite
            return None
        if len(subs) == 1:
            # union of a single sub is the sub itself
            return subs[0]

        # this will raise if any subs are missing times
        subs = sorted(subs)

        output = Subtitle()

        # NOTE that we are not checking for contiguity
        output.start = subs[0].start
        output.end = subs[-1].end

        # concatenate all lines from all subs
        output.lines = list(chain(*[s.lines for s in subs]))

        return output

    def equals(self, other):
        '''Compare with another subtitle and return whether or not both have the
        same times and text.'''
        return (self.start == other.start and self.end == other.end and
                self.lines == other.lines)


class SRTFile:
    '''An SRT file'''

    subtitles = None

    def __init__(self, file_path=None):
        self.subtitles = []

        if file_path:
            self.load(file_path)

    def load(self, file_path):
        '''Load all subtitles in a file.'''
        cur_subtitle = None

        with open(file_path) as f:
            for line in f:
                # strip BOM and newline
                line = line.lstrip('\ufeff').rstrip()

                if cur_subtitle and cur_subtitle.start:
                    # try to read text line
                    if not cur_subtitle.append_line(line):
                        # the current subtitle is over
                        cur_subtitle = None
                elif cur_subtitle:
                    # this is the first line after the subtitle number, which
                    # should be a time line
                    cur_subtitle.read_times(line)
                else:
                    # the current line should be a subtitle number
                    if sub_num_regex.match(line):
                        # start new subtitle
                        cur_subtitle = Subtitle(int(line))
                        self.subtitles.append(cur_subtitle)
                    else:
                        raise ValueError(f'Invalid subtitle number: {line}')

            # sort by time in case the input file is out of order
            self.subtitles = sorted(self.subtitles)

    def drop_empty_subs(self):
        '''Drop empty subtitles.'''
        non_empty = []
        for sub in self.subtitles:
            if sub.lines:
                non_empty.append(sub)
        self.subtitles = non_empty

    def save(self, output_path):
        '''Save subtitles to file.'''

        # make sure we don't try to print empty subtitles, which would raise an
        # error
        self.drop_empty_subs()

        with open(output_path, 'w') as f:
            for i, sub in enumerate(self.subtitles):
                print(i+1, sub, sep='\n', file=f)


class CaptionsCleaner:
    '''Functions for cleaning up closed captions.'''

    _html_tag_regex = re.compile('</?[a-z][^>]*>', re.I)

    _final_punctuation_regex = re.compile('[.?!;~)"*…”’]$')

    max_incomplete_subs = 10

    def __init__(self, max_incomplete_subs):
        if max_incomplete_subs:
            self.max_incomplete_subs = max_incomplete_subs

    def strip_html_tags(self, subtitle):
        '''Strip any HTML tags present in a subtitle's text.'''
        stripped = []
        for line in subtitle.lines:
            if '<' in line:
                line = self._html_tag_regex.sub('', line).strip()
            stripped.append(line)
        subtitle.lines = stripped

    @staticmethod
    def drop_duplicate_lines(subtitle, lines_seen):
        '''Drop duplicate lines from a subtitle.

        NOTE that if all lines are duplicates, the subtitle will be left empty.
        '''
        unique_lines = []

        for i, line in enumerate(subtitle.lines):
            if line not in lines_seen:
                # don't bother checking for further lines, since all duplicates
                # should be in the beginning of the subtitle
                unique_lines = subtitle.lines[i:]
                break

        subtitle.lines = unique_lines

    def clean_up_srt(self, srt):
        '''Clean up all of a file's subtitles.'''

        last_sub_lines = None
        for sub in srt.subtitles:

            self.strip_html_tags(sub)

            # note that we save the lines before de-duplicaton, since we want to
            # check the next subtitles against the original lines and not only
            # against the unique ones
            cur_sub_lines = set(sub.lines)

            if last_sub_lines is not None:
                self.drop_duplicate_lines(sub, last_sub_lines)

            last_sub_lines = cur_sub_lines

    def concatenate_sentences(self, srt):
        '''Concatenate sentences that were broken up into multiple subtitles.'''

        output = []

        # subs containing incomplete portions of a sentence
        incomplete_subs = []

        for sub in srt.subtitles:
            if not sub.lines:
                incomplete_subs.append(sub)
                continue

            # NOTE that multi-line subs that don't end in punctuation will be
            # considered incomplete
            if self._final_punctuation_regex.search(sub.lines[-1]):
                # concatenate all subs
                output.append(Subtitle.union(incomplete_subs + [sub]))
                incomplete_subs = []
            else:
                incomplete_subs.append(sub)

                if len(incomplete_subs) > self.max_incomplete_subs:
                    print(f'Too many subtitles without punctuation found at '
                          f'{sub.start}; aborting concatenation',
                          file=sys.stderr)
                    return srt

        # if the file ended with an incomplete sentence, concat the last subs
        if incomplete_subs:
            output.append(Subtitle.union(incomplete_subs))

        # join all lines in all output subs (i.e. make every sub have a single
        # line of text)
        for sub in output:
            sub.join_lines()

        srt.subtitles = output
        return srt


if __name__ == '__main__':
    _parser = argparse.ArgumentParser(description='Closed caption cleaner')

    _parser.add_argument(
        '-c', '--concat', dest='concat', action='store_true',
        help='Attempt to concatenate sentences that have been broken up into '
        'multiple subtitles.')

    _parser.add_argument(
        '-m', '--max-incomplete-subs', dest='max_incomplete_subs', type=int,
        default=None,
        help='Maximum number of subtitles without punctuation before we give '
        'up on concatenating.')

    _parser.add_argument(
        'files', metavar='files', nargs='+',
        help='SRT files to be cleaned up')

    _args = _parser.parse_args()

    _cleaner = CaptionsCleaner(max_incomplete_subs=_args.max_incomplete_subs)

    for _file_in in _args.files:
        _base_out = ext_regex.sub('', _file_in)
        _file_out = f'{_base_out}_clean.srt'

        _srt = SRTFile(_file_in)

        _cleaner.clean_up_srt(_srt)

        if _args.concat:
            _cleaner.concatenate_sentences(_srt)

        _srt.save(_file_out)
