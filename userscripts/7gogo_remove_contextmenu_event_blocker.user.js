// ==UserScript==
// @name        7gogo - remove contextmenu event blocker
// @namespace   https://gitlab.com/loopvid/scripts
// @description Remove contextmenu event blocker
// @include     https://7gogo.jp/*
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var verbose = false;

  function modified_addeventlistener(event_type, listener_function, options) {
    /* Drop listeners if the event type is contextmenu. */
    /* jshint validthis:true */

    if (event_type == 'contextmenu') {
      if (verbose) {
        console.log('dropping a contextmenu listener');
      }
      return;
    }
    else {
      // add listener normally
      this.realAddEventListener(event_type, listener_function, options);
    }
  }

  function hijack_addeventlistener() {
    /* Replace addEventListener() so we can choose which listeners to let
     * through.
     *
     * Note that 7gogo's context menu blocker listens on document, so we can
     * hijack document.addEventListener() only and avoid unnecessary overhead.
     * If the listeners were being added to the images themselves or to their
     * ancestors, then we could hijack HTMLElement.prototype.addEventListener()
     * instead (or HTMLImageElement, HTMLDivElement, etc, if the element is
     * known).
     * See http://stackoverflow.com/a/6434924 */

    // save reference to original addeventlistener before inserting our own
    document.realAddEventListener = document.addEventListener;
    document.addEventListener = modified_addeventlistener;

    if (verbose) {
      console.log('replaced document.addEventListener');
    }
  }

  hijack_addeventlistener();

}());
