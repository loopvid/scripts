// ==UserScript==
// @name          Tumblr - redirect image to 1280
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects tumblr images of smaller size to 1280px width
// @include       https://*.media.tumblr.com/*tumblr_*_*.*
// @include       http://*.media.tumblr.com/*tumblr_*_*.*
// @version       1.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var images, new_url,
      url = window.location.href,
      width_regex = new RegExp('_([0-9]+)\\.([a-z]{3,4})$'),
      match = width_regex.exec(url);

  if (match && parseInt(match[1]) !== 1280) {

    new_url = url.replace(width_regex, '_1280.'+match[2]);

    // just to be sure
    if (new_url !== url) {
      // get rid of image so it won't load twice
      images = document.getElementsByTagName('img');
      if (images.length > 0) {
        images[0].parentNode.removeChild(images[0]);
      }

      // redirect
      window.location.replace(new_url);
    }
  }

}());
