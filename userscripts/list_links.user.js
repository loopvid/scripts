// ==UserScript==
// @name          List links
// @namespace     https://gitlab.com/loopvid/scripts
// @description   List links in a page
// @match         *://*/*
// @version       2.0.1
// @grant         GM_setValue
// @grant         GM_getValue
// @grant         GM_deleteValue
// @run-at        document-end
// ==/UserScript==

/*
 * Usage:
 *
 * Press ctrl-shift-L to open the search panel on the bottom-right corner of
 * the window.
 *
 * The first input should contain a comma-separated list of the html tag names
 * whose elements you want to search.
 *
 * The second input should contain a comma-separated list of all the attributes
 * whose values should be tested.
 *
 * The third input should contain the search term, which can be a simple
 * substring (or multiple substrings separated by the wildcard character '*')
 * or a regular expression, as controlled by the select below it.
 */

(function() {
  'use strict';

  // don't run on frames
  if (window.top != window.self) { return true; }

  var menu_elem, output_elem,
    // random string for element classes, to avoid conflicts with the page
    random_string = 'rkAf0lcj',
    root_class = 'list_links_container_'+random_string,
    debug_level = 0
  ;

  function remove_menu() {
    if (menu_elem) {
      menu_elem.parentNode.removeChild(menu_elem);
      menu_elem = null;
    }
  }

  function remove_output() {
    if (output_elem) {
      output_elem.parentNode.removeChild(output_elem);
      output_elem = null;
    }
  }

  function get_attr_value(elem, attr) {
    if (attr.indexOf('data-') === 0) {
      // data attribute
      attr = attr.slice('data-'.length);
      return elem.dataset[attr];
    }
    else {
      // regular attribute
      return elem.getAttribute(attr);
    }
  }

  function test_string(string, pattern, pattern_type) {
    /* check if the string matches the pattern */
    var i, found, substrs, idx;

    if (!(string && pattern && pattern_type)) {
      return false;
    }

    // ignore case
    // note that the pattern is already lowercase
    string = string.toLowerCase();

    // if the pattern is a regular expression, simply test the string
    if (pattern_type == 'regex') {
      return string.search(pattern) > -1;
    }

    // split it on all wildcards and test each individual substring
    found = true;
    substrs = pattern.split('*');

    for (i=0; i < substrs.length; i++) {
      // skip empty substrings (for example if the pattern starts or ends with
      // a wildcard or has two in a row)
      if (!substrs[i]) { continue; }

      idx = string.indexOf(substrs[i]);
      if (idx > -1) {
        /* drop the portion of the string that has already matched, so we don't
         * end up matching substrings out of order (e.g.  the string 'abc'
         * should match the pattern 'a*c' but the string 'ca' shouldn't) */
        string = string.slice(idx+substrs[i].length, string.length);
      }
      else {
        found = false;
        break;
      }
    }

    return found;
  }

  function search(params) {
    var i, j, k, pattern, form, elems, value, pattern_type, elem, attr,
        seen = {},
        urls = [];

    console.log('creating regexp');

    pattern = params.pattern;
    pattern_type = params.pattern_type;

    if (pattern_type === 'regex') {
      try {
        pattern = new RegExp(params.pattern, 'i');
      } catch(e) {
        form = menu_elem.querySelector('form');
        form.appendChild(document.createTextNode('Bad pattern'));
        return;
      }
    }
    else {
      pattern = pattern.toLowerCase();
    }

    console.log('searching');

    for (i=0; i < params.elems.length; i++) {
      elems = document.getElementsByTagName(params.elems[i]);

      for (j=0; j < elems.length; j++) {
        elem = elems[j];

        for (k=0; k < params.attrs.length; k++) {
          attr = params.attrs[k];
          if (!attr) { continue; }

          value = get_attr_value(elem, attr);
          if (!value || seen[value]) { continue; }

          // search according to the pattern type
          if (test_string(value, pattern, pattern_type)) {
            // prepend server address if value is an absolute path
            if (value.search('/') === 0) {
              value = (
                window.location.protocol + '//' + window.location.host + value
              );
            }
            urls.push(value);
            seen[value] = true;
          }
        }
      }
    }

    console.log('done');

    remove_menu();
    build_output_elem(params, urls);
  }

  function save_params(params) {
    var saved_params,
      host = window.location.hostname,
      saved_json = GM_getValue('list_links_params');

    saved_params = saved_json ? JSON.parse(saved_json) : {};

    // save the current parameters for the current domain
    saved_params[host] = params;

    if (debug_level >= 1) {
      console.log('saving params');

      if (debug_level >= 2) {
        console.log(JSON.stringify(saved_params));
      }
    }

    GM_setValue('list_links_params', JSON.stringify(saved_params));
  }

  function load_params() {

    var saved_params,
      host = window.location.hostname,
      saved_json = GM_getValue('list_links_params');

    saved_params = saved_json ? JSON.parse(saved_json) : null;

    if (debug_level >= 1) {
      console.log('loaded params');

      if (debug_level >= 2) {
        console.log(JSON.stringify(saved_params));
      }
    }

    if (saved_params && saved_params[host]) {
      return saved_params[host];
    }
    return null;
  }

  function handle_submit(event) {
    /* extract search parameters and perform search */

    if (debug_level >= 2) {
      console.log('handle submit');
    }

    var form,
        inputs = {},
        params = {};

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (menu_elem) {
      form = menu_elem.querySelector('form');
      if (form) {
        inputs.elems = form.querySelector('input[name="elems"]');
        inputs.attrs = form.querySelector('input[name="attrs"]');
        inputs.pattern = form.querySelector('input[name="pattern"]');
        inputs.pattern_type = form.querySelector('select[name="pattern_type"]');
      }
    }

    if (!(inputs.elems && inputs.attrs &&
          inputs.pattern && inputs.pattern_type)) {
      console.log('error finding inputs');
      remove_menu();
      return;
    }

    params.elems = inputs.elems.value.split(',');
    params.attrs = inputs.attrs.value.split(',');
    params.pattern = inputs.pattern.value;
    params.pattern_type = inputs.pattern_type.value;

    if (params.elems.length === 0 || params.attrs.length === 0 ||
        !params.pattern) {
      form.appendChild(document.createTextNode('Please input all values'));
      return;
    }

    save_params(params);

    search(params);
  }

  function confirm_delete_params(event) {
    event.preventDefault();
    event.stopPropagation();

    if(window.confirm('Are you sure you want to clear ALL saved searches?')) {
      if (debug_level >= 1) {
        console.log('clearing saved params');
      }

      GM_deleteValue('list_links_params');

      remove_menu();
    }
  }

  function build_output_elem(params, urls) {
    if (debug_level >= 2) {
      console.log('build output elem');
    }

    var button, textarea;

    if (output_elem) {
      remove_output();
    }

    output_elem = document.createElement('div');

    output_elem.innerHTML = (
      '<div><textarea cols="50" rows="10"></textarea></div>'+
      '<button class="new_search">Search again</button>'+
      '<button class="close">Close</button>'
    );
    output_elem.classList.add(root_class);

    if (debug_level >= 2) {
      console.log('filling out textarea');
    }

    textarea = output_elem.querySelector('textarea');

    if (urls.length > 0) {
      textarea.value = urls.join('\n');
    }
    else {
      textarea.value = 'No results found';
    }

    if (debug_level >= 2) {
      console.log('setting up buttons');
    }

    button = output_elem.querySelector('button.close');
    button.addEventListener('click', remove_output);

    button = output_elem.querySelector('button.new_search');
    button.addEventListener('click', function() {
      remove_output();
      build_menu(params);
    });

    document.body.appendChild(output_elem);
  }

  function build_menu(params) {
    if (debug_level >= 2) {
      console.log('build menu');
    }

    var form;

    if (menu_elem) {
      remove_menu();
    }

    menu_elem = document.createElement('div');
    menu_elem.classList.add(root_class);

    menu_elem.innerHTML = (
      '<form>'+
      '<div class="center_'+random_string+'">Search links</div>'+
      '<div>Elements: <input type="text" name="elems" value="a,img" /></div>'+
      '<div>Attributes: <input type="text" name="attrs" value="href,src" /></div>'+
      '<div>Pattern: <input type="text" name="pattern" value="*.jpg" /></div>'+
      '<input type="submit" />'+
      '<div>Type: <select name="pattern_type">'+
        '<option value="substr" selected>Substring</option>'+
        '<option value="regex">Regular expression</option>'+
      '</select></div>'+
      '</form>'+
      '<div>'+
        '<button class="clear_'+random_string+'">Clear saved data</button>'+
        '<button class="cancel_'+random_string+'">Cancel</button>'+
        '<button class="submit_'+random_string+'">Search</button>'+
      '</div>'
    );

    if (debug_level >= 2) {
      console.log('setting up event listeners');
    }

    form = menu_elem.querySelector('form');
    form.addEventListener('submit', handle_submit);

    menu_elem.querySelector('button.submit_'+random_string).addEventListener(
      'click', handle_submit);

    menu_elem.querySelector('button.cancel_'+random_string).addEventListener(
      'click', remove_menu);

    menu_elem.querySelector('button.clear_'+random_string).addEventListener(
      'click', confirm_delete_params);

    // reinsert previous values, if available
    if (params) {
      if (params.elems) {
        form.querySelector('input[name="elems"]').value = params.elems.join(',');
      }

      if (params.attrs) {
        form.querySelector('input[name="attrs"]').value = params.attrs.join(',');
      }

      if (params.pattern) {
        form.querySelector('input[name="pattern"]').value = params.pattern;
      }

      if (params.pattern_type) {
        form.querySelector(
          'select[name="pattern_type"]').value = params.pattern_type;
      }
    }

    if (debug_level >= 2) {
      console.log('appending child to document.body');
    }

    document.body.appendChild(menu_elem);
  }

  function keydown_handler(event) {
    if (debug_level >= 3) {
    console.log(
      'got keydown: which='+event.which+' char='+event.char+' key='+event.key+
      ' shiftkey='+event.shiftKey+' ctrl='+event.ctrlKey);
    }

    // ctrl-shift-L
    if (!((event.key === 'L' || event.which === 76) &&
          event.shiftKey && event.ctrlKey)) {
      return;
    }

    if (debug_level >= 2) {
      console.log('got shortcut');
    }

    event.preventDefault();
    event.stopPropagation();

    if (!menu_elem) {
      if (debug_level >= 2) {
        console.log('showing menu');
      }

      // remove output element if present
      if (output_elem) {
        remove_output();
      }

      build_menu(load_params());
    }
    else {
      if (debug_level >= 2) {
        console.log('removing menu');
      }

      remove_menu();
    }
  }

  function set_up_css() {
    var i, rules, elem, _class;

    _class = '.' + root_class;

    rules = [
      _class+' { '+
        'display: inline-block; background-color: #fff; right: 15px; '+
        'bottom: 0px; position: fixed; padding: 5px; border: 1px solid #000; '+
        'z-index: 1000; text-align: right; '+
      '}',
      _class+' input, '+_class+' select { '+
        'border: 1px solid #888; color: #000; background-color: #eee; '+
        'border-radius: 4px; padding: 0.1em; margin: 0.1em; '+
      '}',
      _class+' button { '+
        'border: 1px solid #888; color: #000; background-color: #ccc; '+
        'border-radius: 4px; padding: 0.1em; margin: 0.2em; '+
      '}',
      _class+' .center_'+random_string+' { text-align: center; } ',
      _class+'>form>input[type="submit"] { display: none; }'
    ];

    // insert new sheet for our custom rules
    elem = document.createElement('style');
    document.head.appendChild(elem);
    if (!elem.sheet) {
      console.log("Couldn't insert css rules");
      return;
    }

    // insert rules
    for (i=0; i < rules.length; i++) {
      elem.sheet.insertRule(rules[i], i);
    }
  }

  function set_up() {
    set_up_css();

    document.body.addEventListener('keydown', keydown_handler);

    if (debug_level >= 1) {
      console.log('ready to search');
    }
  }

  set_up();

}());
