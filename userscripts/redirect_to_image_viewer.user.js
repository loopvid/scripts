﻿// ==UserScript==
// @name          Redirect to image viewer
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirect images served with content-type: text/html to image viewer
// @include       /^https?://(img\.?)?tenasia\.hankyung\.com/.+/\d+\.jpg$/
// @exclude       /^https?://(img\.?)?tenasia\.hankyung\.com/.+/\d+-\d+x\d+\.jpg$/
// @version       1.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var image_viewer_url = 'https://kastden.org/~lv/image_viewer/';

  function redirect() {
    var new_url = image_viewer_url + '#' + window.location.href;

    window.location.replace(new_url);
  }

  // run immediately since we want to redirect as soon as possible to avoid
  // loading the image for no reason
  redirect();

}());
