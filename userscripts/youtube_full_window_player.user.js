// ==UserScript==
// @name        Youtube - full window player
// @namespace   https://gitlab.com/loopvid/scripts
// @description Allow expanding the player so that it occupies the whole window.
// @include     http*://*.youtube.com/*
// @include     http*://*.dailymotion.com/*
// @include     http*://www.vlive.tv/*
// @include     http*://www.bitchute.com/*
// @include     http*://odysee.com/*
// @include     http*://tv.nrk.no/*
// @version     2.8.5
// @grant       none
// ==/UserScript==

/* USAGE:
 *
 * We will position a small toggle on the top left corner of the screen on
 * video sites' video pages, which when clicked will expand the player to
 * occupy the whole screen or shrink it back to its original size and position.
 * If hotkeys are enabled, you can use 'w' to toggle full window mode and esc
 * to exit it only.
 *
 * When the player is expanded, another small button will be added to the top
 * left corner for rotating the video 90 degrees clockwise. The 'r' hotkey will
 * also become available for the same purpose. Note that rotation only affects
 * the video when expanded, and that it is not preserved when navigating to a
 * different video.
 */

(function() {
  'use strict';

  var website,
    debug_level = 0,
    enable_hotkeys = true,
    last_href = null,
    is_expanded = false,
    rotation = 0,
    hostnames = [
      ['gaming.youtube.com', 'youtube_gaming'],
      ['youtube.com', 'youtube'],
      ['dailymotion.com', 'dailymotion'],
      ['vlive.tv', 'vlive'],
      ['bitchute.com', 'bitchute'],
      ['odysee.com', 'odysee'],
      ['nrk.no', 'nrk']
    ],
    players = {
      youtube: '#movie_player',
      youtube_gaming: '#player',
      dailymotion: '#root>div>div.Player',
      vlive: '#playerBoxArea',
      bitchute: '.video-container .plyr',
      odysee: 'div[class*="content__viewer"]',
      nrk: 'div[class*="video-player"]'
    },
    watch_pages = {
      youtube: 'watch',
      youtube_gaming: 'watch',
      dailymotion: 'video',
      vlive: /post|video/,
      bitchute: 'video',
      odysee: '(/[@][^/]+:[0-9a-f]+)?/[^/]+|/[^/]+:[0-9a-f]+$',
      nrk: 'avspiller'
    },
    // site specific css
    css_rules = {
      // note that youtube_gaming will be set in set_up_css()
      youtube: [
        'body.player_expanded app-header, '+
          'body.player_expanded #masthead-positioner, '+
          'body.player_expanded #masthead-container, '+
          'body.player_expanded #ticket-shelf, '+
          'body.player_expanded #merch-shelf, '+
          'body.player_expanded ytd-popup-container, '+
          'body.player_expanded .yt-dialog-base '+
          '{ display: none !important; }',
        '.full_window_player { background-color: #000; }',
        '.full_window_player .html5-video-container { height: 100%; }',
        '.full_window_player .ytp-chrome-bottom { left: 50% !important; '+
          'transform: translate(-50%, 0px); }',
        '.full_window_player video { '+
          /* for vertical videos and full screen, youtube will try to use top
           * and left to center the video, which we must override in full
           * window mode */
          'top: 0px !important; left: 0px !important; '+
          /* youtube sets object-fit: cover; which causes the full window video
           * to overflow */
          'object-fit: contain !important; '+
        '}',
        'body.player_expanded #opt-out-dialog, '+
          'body.player_expanded yt-live-chat-header-renderer, '+
          'body.player_expanded .watch-sidebar button { display: none; }'
      ],
      dailymotion: [
        'body.player_expanded header, '+
          'body.player_expanded div[class*="CookiePopup"] { display: none; }',
        'body.player_expanded .dmp_VideoView-content { '+
          'top: 0px !important; left: 0px!important; width: 100vw !important; '+
          'height: 100vh !important; }'
      ],
      vlive: [
        'body.player_expanded div[class*="layout_main"]>div[class*="snb"] '+
          '{ display: none !important; }'
      ],
      bitchute: [
        'body.player_expanded #nav-menu, '+
          'body.player_expanded #alert-signup { display: none; }',
        'body.player_expanded .plyr__video-wrapper { '+
          'padding-bottom: 0 !important; height: 100% !important; }'
      ],
      odysee: [
        'body.player_expanded header { display: none; }',
        'body.player_expanded .main-wrapper__inner { margin-top: 0 ! important }',
        '.full_window_player { max-height: none !important; '+
          'border-radius: 0 !important; }',
        '.full_window_player .file-render--video, .full_window_player video { '+
          'height: 100vh; max-height: none; }',
        '.full_window_player video { max-height: none !important; }'
      ],
      nrk: [
        '.full_window_player { border-radius: 0; }',
        '.full_window_player>.ludo { height: 100vh; }'
      ]
    };

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  function get_player() {
    var selector = players[website];
    if (!selector) { return null; }
    return document.querySelector(selector);
  }

  function expand_player() {
    /* expand the player to fill the whole window */
    var button = document.getElementById('full_window_enter_exit_button'),
        player = get_player();

    if (!button) {
      console.log(
        'attempt to expand player without buttons in place; '+
        'will try to recover'
      );

      destroy_buttons();
      create_buttons();
      button = document.getElementById('full_window_enter_exit_button');

      if (!button) {
        console.log('failed to re-create buttons; aborting');
        return;
      }
    }

    player.classList.add('full_window_player');
    document.body.classList.add('player_expanded');

    // focus on player to prevent youtube bugs when the focus is elsewhere
    player.focus();

    button.className = 'full_window_exit';
    button.innerHTML = '↮';

    is_expanded = true;
  }

  function shrink_player() {
    /* shrink full window player back to its normal size */
    var button = document.getElementById('full_window_enter_exit_button'),
        player = get_player();

    document.body.classList.remove('player_expanded');

    if (player) {
      player.classList.remove('full_window_player');
    }

    if (button) {
      button.className = 'full_window_enter';
      button.innerHTML = '↔';
    }

    is_expanded = false;
  }

  function rotate_video() {
    var player = get_player();

    if (!(document.body.classList.contains('player_expanded') && player)) {
      return;
    }

    // undo previous rotation
    if (rotation) {
      player.classList.remove('rot'+rotation);
    }

    // rotate 90 degrees clockwise
    rotation += 90;
    rotation %= 360;

    // apply new rotation (do nothing if we're back at 0 degrees)
    if (rotation) {
      player.classList.add('rot'+rotation);
    }
  }

  function undo_rotation() {
    var player = get_player();

    if (rotation && player && player.classList.contains('rot'+rotation)) {
      player.classList.remove('rot'+rotation);
    }

    rotation = 0;
  }

  function expand_button_click_handler(event) {
    // only capture unmodified clicks with the left button
    if (event.button !== 0 || event.ctrlKey || event.altKey || event.shiftKey ||
        event.metaKey) {
      return;
    }

    if (is_expanded) {
      shrink_player();
    }
    else {
      expand_player();
    }
  }

  function rotate_button_click_handler(event) {
    // only capture unmodified clicks with the left button
    if (event.button !== 0 || event.ctrlKey || event.altKey || event.shiftKey ||
        event.metaKey) {
      return;
    }

    rotate_video();
  }

  function keydown_handler(event) {
    /* check if key is one of our shortcuts and, if so, handle it */

    if (!is_watch_page()) {
      // make sure we exited full window mode
      if (is_expanded) {
        shrink_player();
      }
      return;
    }

    // never capture a key from an input; never capture modified key presses
    if (event.target.tagName === 'INPUT' ||
        event.target.tagName == 'TEXTAREA' ||
        event.ctrlKey || event.altKey || event.shiftKey || event.metaKey) {
      return;
    }

    // toggle full [w]indow mode
    if (event.key == 'w' || event.keyCode == 87) {
      // toggle full window mode
      if (is_expanded) {
        shrink_player();
      }
      else {
        expand_player();
      }
    }
    // [escape] full window mode
    else if (is_expanded && (event.key == 'Escape' || event.keyCode == 27)) {
      // exit full window mode
      shrink_player();
    }
    // [r]otate
    else if (is_expanded && (event.key == 'r' || event.keyCode == 82)) {
      rotate_video();
    }
    // not one of our hotkeys, just ignore it
    else { return; }

    // stop propagation (only if we did handle the event)
    event.preventDefault();
    event.stopPropagation();
  }

  function create_buttons() {
    if (document.getElementById('full_window_buttons')) { return; }

    var outer_div, button;

    outer_div = document.createElement('DIV');
    outer_div.id = 'full_window_buttons';

    button = document.createElement('DIV');
    button.className = 'full_window_enter';
    button.innerHTML = '↔';
    button.id = 'full_window_enter_exit_button';
    button.addEventListener('click', expand_button_click_handler);
    outer_div.appendChild(button);

    button = document.createElement('DIV');
    button.className = 'full_window_rotate';
    button.innerHTML = '↷';
    button.id = 'full_window_rotate_button';
    button.addEventListener('click', rotate_button_click_handler);
    outer_div.appendChild(button);

    document.body.appendChild(outer_div);
  }

  function destroy_buttons() {
    var buttons = document.getElementById('full_window_buttons');
    if (buttons) {
      buttons.parentNode.removeChild(buttons);
    }
  }

  function is_watch_page() {
    /* check if this is a watch page */
    var path = watch_pages[website];

    if (!path) {
      console.log('error determining watch page path');
      return false;
    }

    return window.location.pathname.search(path) >= 0;
  }

  function maintain_body_class() {
    /* if in full window mode, check that the body still has the
     * player_expanded class (since youtube's js wipes it on page changes) */
    if (is_expanded && !document.body.classList.contains('player_expanded')) {
      document.body.classList.add('player_expanded');
    }
  }

  function set_up_button() {
    // don't check again until we navigate to a different url
    if (window.location.href == last_href) { return; }

    if (debug_level >= 1) {
      console.log('checking new url '+window.location.href+
                  ' last was '+last_href);
    }

    if (is_watch_page()) {
      // don't check again for this page
      last_href = window.location.href;

      // if button doesn't already exist, create it
      if (!document.getElementById('full_window_buttons')) {
        create_buttons();
      }

      /* if we're currently in full window mode, re-run expand_player() to make
       * sure our classes will continue to be in place */
      if (is_expanded) {
        expand_player();
      }

      // undo rotation, if applicable, since we don't want to preserve it
      // across different videos
      undo_rotation();
    }
    else { // not a watch page
      last_href = window.location.href;

      destroy_buttons();

      /* if we're currently in full window mode, exit it */
      if (is_expanded) {
        shrink_player();
      }
    }
  }

  function set_up_css() {
    /* insert new sheet for our custom rules */
    var elem, rules, i, transforms, zindex;

    // youtube gaming uses the same css as regular youtube
    css_rules.youtube_gaming = css_rules.youtube;

    rules = css_rules[website];
    if (!rules) {
      console.log('error finding site specific css');
      return;
    }

    elem = document.createElement('style');
    document.head.appendChild(elem);

    transforms = {
      '90': 'translate(calc(50vw - 50vh), calc(50vh - 50vw)) rotate(90deg)',
      '180': 'rotate(180deg)',
      '270': 'translate(calc(50vw - 50vh), calc(50vh - 50vw)) rotate(270deg)'
    };

    /* NOTE: The new youtube interface's header has a z-index of 2020 (as
     * of 2020), which is higher than all the other websites, but the old
     * interface (which still works for some user-agents) has an unreasonably
     * higher z-index, so we'll try to detect it and apply our own unreasonable
     * value only in that case.
     *
     * In regards to the player, the highest z-index we have to clear is 1000 for
     * youtube's <ytd-channel-name> elements. */
    zindex = {
      buttons: 3000,
      player: 2000
    };
    if (document.querySelector('div#yt-masthead')) {
      zindex.buttons = 2000000000;
    }

    rules = rules.concat([
        'body.player_expanded { overflow: hidden; height: 100vh; }',
        'body.player_expanded iframe { display: none !important; } ',
        '#full_window_buttons { vertical-align: middle; position: fixed; '+
          'left: 4px; top: 0px; opacity: .3; font-size: large; '+
          'cursor: pointer; width: 1em; color: #888; }',
        '#full_window_buttons:hover { opacity: 1; }',
        '#full_window_buttons {  z-index: '+zindex.buttons+'; }',
        '#full_window_enter_exit_button { display: inline-block; }',
        '#full_window_rotate_button { display: none; }',
        '.player_expanded #full_window_rotate_button { display: inline-block; }',

        // dimensions and tranforms for rotations
        '.full_window_player { z-index: '+zindex.player+' !important; '+
          'position: fixed !important; '+
          'top: 0px !important; left: 0px !important; '+
          'width: 100vw !important; height: 100vh !important; }',
        '.full_window_player.rot90 video { transform: '+transforms['90']+'; }',
        '.full_window_player.rot180 video { transform: '+transforms['180']+'; }',
        '.full_window_player.rot270 video { transform: '+transforms['270']+'; }',
        '.full_window_player:not(.rot90):not(.rot270) video { '+
          'width: 100vw !important; height: 100vh !important; }',
        // invert width and height
        '.full_window_player.rot90 video, .full_window_player.rot270 video { '+
          'width: 100vh !important; height: 100vw !important; }'
    ]);

    for (i=0; i < rules.length; i++) {
      elem.sheet.insertRule(rules[i], i);
    }
  }

  function set_up_hotkeys() {
    /* set up keyboard shortcuts for the full window player */

    if (!enable_hotkeys) { return; }

    // NOTE that we use keydown rather than keypress, because pressing Escape
    // on chrome does not fire a keypress event
    document.addEventListener('keydown', keydown_handler);
  }

  function set_up_mutation_observers() {
    var observer;

    /* observe document for changes in the page so we know when we're going
     * into and out of watch pages */
    observer = new MutationObserver(function() { set_up_button(); });
    observer.observe(document, {childList: true, subtree: true});

    /* observe the body for attribute changes, so we know if youtube's js has
     * wiped out our own class */
    observer = new MutationObserver(maintain_body_class);
    observer.observe(document.body, {attributes: true});
  }

  function set_up_website() {
    /* figure out what website this is */
    var i, hostname = window.location.hostname;

    for (i=0; i < hostnames.length; i++) {
      if (hostname.indexOf(hostnames[i][0]) >= 0) {
        website = hostnames[i][1];
        return;
      }
    }
  }

  function initial_setup() {
    set_up_website();

    if (!website) {
      console.log('error identifying what website this is');
      return;
    }

    set_up_mutation_observers();
    set_up_css();
    set_up_hotkeys();
    set_up_button();
  }

  initial_setup();

}());
