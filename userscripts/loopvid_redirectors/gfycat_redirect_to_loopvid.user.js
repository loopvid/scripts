// ==UserScript==
// @name          gfycat - redirect to loopvid
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirect gfycat videos to loopvid
// @include       https://gfycat.com/*
// @include       http://gfycat.com/*
// @include       https://www.gfycat.com/*
// @include       http://www.gfycat.com/*
// @version       1.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var new_url, path = window.location.pathname;

  // check if actual video page
  if (!/^\/([A-Z][a-z]+){3}$/.test(path)) { return; }

  function stop_video(video) {
    video.pause();
    // remove sources
    while (video.firstChild) {
      video.removeChild(video.firstChild);
    }
    // remove video
    video.parentNode.removeChild(video);
  }

  // get rid of video so it won't load twice
  var videos = document.getElementsByTagName('video');
  if (videos.length > 0) {
    stop_video(videos[0]);
  }

  new_url = 'https://loopvid.appspot.com/#gc' + path;
  window.location.replace(new_url);

}());
