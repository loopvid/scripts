// ==UserScript==
// @name        Youtube - add transforms to video
// @namespace   https://gitlab.com/loopvid/scripts
// @description Use the mouse to scale and translate the video in order to hide borders
// @include     https://www.youtube.com/*
// @version     1.1.1
// @grant       none
// ==/UserScript==

/* USAGE:
 *
 * CLICK and DRAG the video with the MIDDLE MOUSE BUTTON while holding SHIFT in
 * order to move the image.
 *
 * CLICK and DRAG the video with the MIDDLE MOUSE BUTTON while holding CONTROL
 * in order to scale the image.
 *
 *
 * Drag AWAY from the top-left corner (i.e. towards bottom-right) to GROW the
 * image.
 *
 * Drag TOWARDS the top-left corner (i.e. away from bottom-right) to SHRINK the
 * image.
 */

(function () {
  'use strict';

  var debug = false,
      scale_step = 0.008,
      translate_step = 0.2,
      update_delay = 50,  // ms
      //
      last_href = null,
      sheet, transform_rule_num,
      video_id = 'video_with_transform',
      container_id = 'video_with_transform_container',
      operation = null,
      cur_scale_x = 1.0, cur_scale_y = 1.0,
      cur_translate_x = 0.0, cur_translate_y = 0.0,
      initial_x, initial_y,
      dragging = false, dragged = false,
      waiting = false,
      maintain_aspect = true,
      video = null;

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  function get_transform_rule(params) {
    /* generate CSS rule for transform */

    var rule = ['#', video_id, ' { transform: ',
      'scale(', params.scale_x, ', ', params.scale_y,') ',
      'translate(', params.translate_x, '%, ', params.translate_y, '%) ',
      '!important; }'].join('');

    return rule;
  }

  function apply_transform(params) {
    var rule;

    if (!params) {
      // reset transform
      rule = get_transform_rule(1.0, 1.0, 0.0, 0.0);
    }
    else {
      rule = get_transform_rule(params);
    }

    sheet.deleteRule(transform_rule_num);
    sheet.insertRule(rule , transform_rule_num);
  }

  function get_transform_params(event) {
    /* calculate new transform parameters */
    var params, dif_x, dif_y,
        scale_x, scale_y, translate_x, translate_y;

    dif_x = event.screenX - initial_x;
    dif_y = event.screenY - initial_y;

    // calculate new transform
    scale_x = cur_scale_x;
    scale_y = cur_scale_y;
    translate_x = cur_translate_x;
    translate_y = cur_translate_y;

    if (operation === 'scale') {
      if (maintain_aspect) {
        scale_x = scale_y = cur_scale_x + scale_step*(dif_x+dif_y);
      }
      else {
        scale_x = cur_scale_x + scale_step*dif_x;
        scale_y = cur_scale_y + scale_step*dif_y;
      }
    }
    else {
      translate_x = cur_translate_x + translate_step*dif_x;
      translate_y = cur_translate_y + translate_step*dif_y;
    }

    params = {
      scale_x: scale_x,
      scale_y: scale_y,
      translate_x: translate_x,
      translate_y: translate_y
    };

    return params;
  }

  function mouseup_handler(event) {
    /* stop watching mousemove */
    var params;
    if (!dragging) { return; }

    document.body.removeEventListener('mousemove', mousemove_handler);

    if (debug) { console.log('mouseup'); }

    if (dragged) {
      dragging = false;
      dragged = false;

      // save current parameters
      params = get_transform_params(event);
      cur_scale_x = params.scale_x;
      cur_scale_y = params.scale_y;
      cur_translate_x = params.translate_x;
      cur_translate_y = params.translate_y;

      // stop the even so youtube won't pause/unpause
      event.preventDefault();
      event.stopImmediatePropagation();
    }
  }

  function mousedown_handler(event) {
    /* start watching mouse movement */
    if (debug) { console.log('got button '+event.button); }

    // only grab middle button
    if (event.button !== 1) { return; }

    // only grab clicks modified by shift or ctrl
    // also don't allow ctrl and shift at the same time
    if (!(event.ctrlKey ^ event.shiftKey) ||
        event.altKey || event.metaKey) { return; }

    // store which operation we're doing
    if (event.ctrlKey) {
      operation = 'scale';
    }
    else if (event.shiftKey) {
      operation = 'translate';
    }

    // store inital mouse coordinates
    initial_x = event.screenX;
    initial_y = event.screenY;

    if (debug) { console.log('mousedown at '+initial_x+'x'+initial_y); }

    // start watching mousemove
    document.body.addEventListener('mousemove', mousemove_handler);
    dragging = true;
    dragged = false;
  }

  function mousemove_handler(event) {
    var params, dif_x, dif_y, tolerance = 3;

    //if (debug) { console.log('mousemove'); }

    if (!dragging) { return; }

    if (waiting) { return; }

    dif_x = event.screenX - initial_x;
    dif_y = event.screenY - initial_y;

    if (!dragged &&
        (Math.abs(dif_x) > tolerance || Math.abs(dif_y) > tolerance)) {
      dragged = true;
    }

    if (dragged) {
      params = get_transform_params(event);

      apply_transform(params);

      // signal a delay so we don't re-render too often
      waiting = true;
      window.setTimeout(function() { waiting = false; }, update_delay);


      event.preventDefault();
      event.stopPropagation();
    }
  }

  function setup_video() {
    /* restore starting values and add event listeners to new video */
    var videos;

    if (debug) { console.log('setting up video'); }

    // reset values
    cur_scale_x = 1; cur_scale_y = 1;
    cur_translate_x = 0; cur_translate_y = 0;
    apply_transform();

    // find new video
    videos = document.getElementsByTagName('VIDEO');
    if (!videos) { return;}

    video = videos[0];
    video.id = video_id;

    // set event listeners
    video.addEventListener('mousedown', mousedown_handler);
    document.body.addEventListener('mouseup', mouseup_handler);

    if (debug) { console.log('finished setting up video'); }
  }

  function setup() {
    if (window.location.href === last_href) { return; }
    last_href = window.location.href;

    if (window.location.pathname.search('watch') >= 0) {
      setup_video();
    }
    else {
      video = null;
    }
  }

  function setup_css() {
    /* insert new sheet for our custom rules */
    var rule, elem;

    elem = document.createElement('style');
    document.head.appendChild(elem);
    sheet = elem.sheet;

    transform_rule_num = 0;

    rule = get_transform_rule(1.0, 1.0, 0.0, 0.0);
    sheet.insertRule(rule, transform_rule_num);
    sheet.insertRule('#'+container_id+' { overflow: hidden; }', 1);
  }

  function initial_setup() {
    var observer = new MutationObserver(setup);
    observer.observe(document, { childList: true, subtree: true });

    setup_css();

    setup();
  }

  initial_setup();

}());
