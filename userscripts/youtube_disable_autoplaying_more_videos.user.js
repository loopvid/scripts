// ==UserScript==
// @name          Youtube - disable autoplaying more videos
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Automatically disable autoplaying another video once the one you're watching ends
// @include       https://www.youtube.com/*
// @include       https://www.vlive.tv/*
// @version       2.1.2
// @grant         none
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  var website,
    last_href = null,
    click_locked = null,
    toggles = {
      youtube: 'button[data-tooltip-target-id="ytp-autonav-toggle-button"]',
      vlive: 'div[class*="auto_playing"]>label[class*="button_switch"]>input'
    },
    toggle_enabled_attribs = {
      youtube: ['aria-label', 'Autoplay is on', false],
      vlive: ['class', 'is_checked', true]
    },
    watch_pages = {
      youtube: 'watch',
      vlive: /post|video/
    };

  function do_click(elem) {
    /* dispatch a click on an element */
    var evt = new MouseEvent(
      'click', { bubbles: true, cancelable: true, view: window});

    elem.dispatchEvent(evt);
  }

  function is_watch_page() {
    /* check if this is a watch page */
    var path = watch_pages[website];

    if (!path) {
      console.log('error determining watch page path');
      return false;
    }

    return window.location.pathname.search(path) >= 0;
  }

  function is_toggle_enabled(elem) {
    /* check if the autoplay toggle is enabled */
    if (!elem) { return false; }

    var pair, attrib, value, check_parent;
    pair = toggle_enabled_attribs[website];
    attrib = pair[0];
    value = pair[1];
    check_parent = pair[2];

    if (check_parent) {
      elem = elem.parentNode;
    }

    return elem && elem.getAttribute(attrib).search(value) > -1;
  }

  function check_autoplay(force) {
    /* if the autoplay checkbox is checked, uncheck it */

    // don't check again until we navigate to a different url
    if (window.location.href === last_href && !force) { return; }

    // nothing to do if this is not a watch page
    if (!is_watch_page()) {
      last_href = window.location.href;
      return;
    }

    var toggle_selector = toggles[website],
      toggle = document.querySelector(toggle_selector);

    // if we can't find the toggle, the page is probably still being loaded, so
    // keep observing it
    if (toggle) {
      // don't check again until we navigate to a different url
      last_href = window.location.href;

      if (is_toggle_enabled(toggle) && !click_locked) {
        // make sure we don't queue up several click attempts while we wait
        click_locked = true;

        console.log('disabling autoplay');
        window.setTimeout(function() {
          // check again to make sure we don't accidentally re-enable the toggle
          if (is_toggle_enabled(toggle)) {
            do_click(toggle);
          }

          click_locked = false;

          // check again in a bit to see if the click got through
          window.setTimeout(function() { check_autoplay(true); }, 1000);
        }, 500);
      }
    }
  }

  function mutation_handler() {
    check_autoplay(false);
  }

  function set_up_mutation_observer() {
    /* keep watching the page for mutations and disable the autoplay toggle
     * when nedded */
    var observer = new MutationObserver(mutation_handler);
    observer.observe(document, { childList: true, subtree: true });

    window.setTimeout(check_autoplay, 1000);
  }

  function set_up_website() {
    /* figure out what website this is */
    var hostname = window.location.hostname;

    if (hostname.search('youtube') >= 0) {
      website = 'youtube';
    }
    else if (hostname.search('vlive') >= 0) {
      website = 'vlive';
    }
  }

  function initial_setup() {
    set_up_website();

    if (!website) {
      console.log('error identifying what website this is');
      return;
    }

    set_up_mutation_observer();
  }

  if (document.readyState == 'complete') {
    initial_setup();
  }
  else {
    window.addEventListener('load', initial_setup);
  }

}());
