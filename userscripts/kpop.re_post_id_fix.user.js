// ==UserScript==
// @name        kpop.re post id fix
// @namespace   https://gitlab.com/loopvid/scripts
// @description Make post links actually select posts
// @include     /^https?://kpop.re/[^/]+/\d+/
// @version     1.1.1
// @grant       none
// ==/UserScript==

(function() {
  'use strict';

  var debug_level = 1;

  function fix_post_link(post) {
    var match,
      regex = /[#](\d+)/,
      link = post.querySelector(
        '.post-header a.post-id:not([data-id_fixed="true"])');

    if (link) {
      match = regex.exec(link.href);

      if (match) {
        if (debug_level >= 2) {
          console.log('fixing link: '+link.href+' -> #post'+match[1]);
        }

        link.href = '#post'+match[1];
      }

      link.dataset.id_fixed = 'true';
    }
  }

  function handle_new_posts(mutations) {
    var i, j, new_nodes, node, parent_node,
      posts_found = 0;

    for (i=0; i < mutations.length; i++) {
      if (!(mutations[i].type === 'childList' && mutations[i].addedNodes &&
            mutations[i].addedNodes.length > 0)) {
        continue;
      }

      new_nodes = mutations[i].addedNodes;
      for (j=0; j < new_nodes.length; j++) {
        node = new_nodes[j];
        parent_node = node ? node.parentNode : null;

        if (!(node && parent_node && node.tagName == 'ARTICLE' &&
              node.classList.contains('post'))) { continue; }

        if (debug_level >= 2) {
          console.log('got new post with id '+node.id);
        }

        posts_found++;

        fix_post_link(node);
      }
    }

    if (posts_found > 0) {
      if (debug_level >= 1) {
        console.log('fixed '+posts_found+' new posts');
      }
    }
  }

  function set_up_css() {
    var i, elem, rules;

    rules = [
      '.post:target { border: 1px solid #555; }'
    ];

    elem = document.createElement('style');
    document.head.appendChild(elem);

    for (i=0; i < rules.length; i++) {
      elem.sheet.insertRule(rules[i], i);
    }
  }

  function set_up_mutation_observer() {
    var observer = new MutationObserver(handle_new_posts),
      thread_elem = document.querySelector('article.thread');

    observer.observe(thread_elem, {childList: true, subtree: true});
  }

  function fix_initial_posts() {
    var i,
      posts = document.querySelectorAll('article.post');

    if (debug_level >= 1) {
      console.log('found '+posts.length+' initial posts');
    }

    for (i=0; i < posts.length; i++) {
      fix_post_link(posts[i]);
    }

    if (debug_level >= 1) {
      console.log('initial posts fixed');
    }
  }

  function initial_setup() {
    if (debug_level >= 1) {
      console.log('setting up');
    }

    set_up_css();
    set_up_mutation_observer();
    fix_initial_posts();

    if (debug_level >= 1) {
      console.log('done setting up');
    }
  }

  initial_setup();

}());
