// ==UserScript==
// @name          video files - add controls
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Adds controls for video files opened directly (that is, opened in a new tab or window, not embedded in a page).
// @include       *
// @version       2.6
// @grant         none
// ==/UserScript==

/* NOTE: this script checks if the current page consists of a single video
 * before doing anything else, so it's okay to run it for all pages. The reason
 * for this is that many videos are served through urls that do not end in
 * .webm or .mp4, so this is the only way to know for sure. */

(function () {
  'use strict';

  /* options */
  var debug = false,
      background_color = '#181818',
      loop_audio = true;

  function setup_speed_controls() {
    var video = document.getElementById('video');
    var speed_bar = document.getElementById('speed_bar');
    var speed_value = document.getElementById('speed_value');

    var playback_speed = 1;
    function adjust_playback_speed() {
      video.playbackRate = playback_speed;

      if (speed_bar) {
        if (speed_bar.value != playback_speed*100) {
          speed_bar.value = playback_speed*100;
        }
      }

      if (speed_value) {
        speed_value.innerHTML = Math.round(playback_speed*100)+'%';
      }
    }
    // adjust to the initial speed
    adjust_playback_speed();

    /* there's a bug in firefox where if you pause the video and play again, it
     * will go back to 100% speed, so we have to reset the speed every time */
    if (navigator.userAgent.match(/\bfirefox\b/i)) {
      video.addEventListener('play', adjust_playback_speed);
    }

    function speed_bar_input_handler() {
      playback_speed = speed_bar.value/100;
      adjust_playback_speed();
    }

    if (speed_bar) {
      // adjust speed as the user moves the speed bar
      speed_bar.addEventListener('input', speed_bar_input_handler);
      // just to make sure, adjust on change as well
      speed_bar.addEventListener('change', speed_bar_input_handler);
    }

    var speed_normal = document.getElementById('speed_normal');
    if (speed_normal) {
      speed_normal.addEventListener('click', function() {
        // back to 100%
        playback_speed = 1;
        adjust_playback_speed();
      });
    }
  }

  function setup_zoom_controls() {
    var video = document.getElementById('video'),
        video_container = document.getElementById('video_container'),
        footer = document.getElementById('footer'),
        zoom_bar = document.getElementById('zoom_bar'),
        zoom_value = document.getElementById('zoom_value'),
        reload_button = document.getElementById('reload_button'),
        rotate_link = document.getElementById('rotate_link');

    function update_debug_div() {
      if (debug) {
        var debug_div = document.getElementById('debug_div');
        if (!debug_div) { return; }
        if (debug_div.style.display === 'none') { return; }

        var video_left = parseInt(video_container.style.left, 10);
        var video_top = parseInt(video_container.style.top, 10);

        debug_div.innerHTML = '';
        // original dimensions
        debug_div.innerHTML += video.dataset.width+','+video.dataset.height;
        // scale
        debug_div.innerHTML += '*'+new_scale.toFixed(2);
        // rotation
        debug_div.innerHTML += ' ('+rotation*90+'deg)';
        // scaled dimensions
        debug_div.innerHTML += ' = '+Math.round(video.dataset.width*new_scale)+','+Math.round(video.dataset.height*new_scale);
        // translation and centering
        debug_div.innerHTML += ':'+translate_x+'(';
        if (video_left >= 0) debug_div.innerHTML += '+';
        debug_div.innerHTML += video_left+')';
        debug_div.innerHTML += ','+translate_y+'(';
        if (video_top >= 0) debug_div.innerHTML += '+';
        debug_div.innerHTML += video_top+')';
        // window dimensions and (scrolling) position
        debug_div.innerHTML += ' @ '+window.innerWidth;
        debug_div.innerHTML += '(+'+document.body.scrollLeft+')';
        debug_div.innerHTML += ','+window.innerHeight;
        debug_div.innerHTML += '(+'+document.body.scrollTop+')';
      }
    }
    if (debug) {
      window.addEventListener('scroll', update_debug_div);
    }

    function reload_video() {
      /* The video element here is not like a regular video in a page (it
       * lacks a source, for example), so we can't just reload it.
       * Instead we must replace it with a brand new video element.*/

      // create new video using the current url as source
      var source_src = document.location.href;
      var new_video = document.createElement('video');
      var new_source = document.createElement('source');
      new_source.src = source_src;
      new_video.appendChild(new_source);
      video.parentNode.replaceChild(new_video, video);

      // setup video attributes
      new_video.id = 'video';
      new_video.controls = 'controls';
      new_video.autoplay = 'autoplay';
      new_video.loop = 'loop';
      new_video.dataset.width = video.videoWidth;
      new_video.dataset.height = video.videoHeight;
      new_video.style.maxWidth = video.videoWidth+'px';
      new_video.style.maxHeight = video.videoHeight+'px';

      // setup controls for new video
      wait_for_video();

      // load video
      new_video.load();

      video = new_video;
    }
    reload_button.addEventListener('click', reload_video);

    function rotate_clockwise(event) {
      event.stopPropagation();
      event.preventDefault();

      // add 90 degrees to the current rotation
      rotation = (rotation + 1) % 4;

      // switch width and height
      if (rotation % 2 === 0) {
        video.dataset.width = video.videoWidth;
        video.dataset.height = video.videoHeight;
      }
      else {
        video.dataset.width = video.videoHeight;
        video.dataset.height = video.videoWidth;
      }

      // redraw the video
      scale_and_position(true);
    }
    rotate_link.addEventListener('click', rotate_clockwise);

    var transforms = ['transform', 'WebkitTransform', 'MozTransform', 'msTransform', 'OTransform'];
    var i, transform;
    for (i=0; i < transforms.length; i++) {
      if(video.style[transforms[i]] !== undefined){
        transform = transforms[i];
        break;
      }
    }

    /* initial scale */
    var start_scale = 1;
    var new_scale = 1;

    var translate_x = 0;
    var translate_y = 0;

    var rotation = 0; // times 90 degrees

    // to be set during mouse down
    var initial_scroll_left = 0;
    var initial_scroll_top = 0;

    // higher mouse_video_delta_multiplier means faster scaling.
    // (roughly, mouse_distance_moved*mouse_video_delta_multiplier = change_in_video_dimensions)
    var mouse_video_delta_multiplier = 3;

    function get_page_height(scale) {
      /* return page height at a certain scale:
       * scaled video height + footer height + scrollbar */
      var scrollbar = 0;
      if (video.dataset.width*scale > window.innerWidth) {
        scrollbar = 17; // de-facto standard size for browser scrollbars
      }
      if (footer) {
        return Math.round(video.dataset.height*scale + footer.clientHeight + scrollbar);
      }
      return Math.round(video.dataset.height*scale + scrollbar);
    }

    function calculate_video_translation() {
      /* keep video's top-left corner in place (at container's top-left)
       * during scaling */

      var dif = video.videoHeight-video.videoWidth,
          tx = Math.round(video.videoWidth*(1-1/new_scale)/2),
          ty = Math.round(video.videoHeight*(1-1/new_scale)/2);

      // without rotation, we only have to compensate for the scaling
      if (rotation === 0) {
        translate_x = tx;
        translate_y = ty;
      }
      // when rotating, also compensate for difference between width and height
      // and invert translations when applicable
      else if (rotation === 1) {
        // 90deg = ty-dif/2,-tx-dif/2
        translate_x = ty-dif/2;
        translate_y = -tx-dif/2;
      }
      else if (rotation === 2) {
        // 180deg = -tx,-ty
        translate_x = -tx;
        translate_y = -ty;
      }
      else {
        // 270deg = -(ty-dif/2),-(-tx-dif/2)  (the opposite of 90deg)
        translate_x = -ty+dif/2;
        translate_y = tx+dif/2;
      }
    }

    function scale_and_position(is_jump) {
      /* To keep the video centered at all times, we compensate its movement
       * during scaling with an appropriate translation to keep its top-left
       * corner in place, and we set the container's left in order to center it
       * while the video is still smaller than the window (its top remanining
       * always in place). After a dimension grows bigger than the window's, we
       * center the window instead, via scrolling.
       *
       * Notice that we need the video container in order to have the
       * document's body only see an element the size of the scaled video.
       * Without the container and with a large downscaled video, the body will
       * grow enough to contain the original video's dimensions, leaving blank
       * space to its right and/or bottom. Since the container's dimensions
       * follow the scaled video's and its overflow is hidden, we get the exact
       * same behavior from it as we would get from a small video the size of
       * the downscaled video, which is precisely what we want. */

      if (zoom_value) {
        zoom_value.innerHTML = Math.round(new_scale*100)+'%';
      }
      if (zoom_bar && zoom_bar.value !== new_scale*100) {
        // enforce minimum and maximum values
        if (new_scale > 2 && zoom_bar.value < 200) {
          zoom_bar.value = 200;
        }
        else if (new_scale < 0.1 && zoom_bar.value > 10) {
          zoom_bar.value = 10;
        }
        else {
          zoom_bar.value = new_scale*100;
        }
      }

      var new_width = video.dataset.width*new_scale;
      var new_height = video.dataset.height*new_scale;

      calculate_video_translation();

      // note that changing the order of the operations makes the signs of the
      // operands in calculate_video_translation() different
      video.style[transform] = (
          'scale('+new_scale+','+new_scale+') '+
          'rotate('+rotation*90+'deg) '+
          'translate('+translate_x+'px,'+translate_y+'px)'
          );

      // fit container to scaled video
      video_container.style.width = Math.round(new_width)+'px';
      video_container.style.height = Math.round(new_height)+'px';

      /* center container, but only until it hits the window's boundaries;
       * after that, center window instead */
      var scroll_left = initial_scroll_left;
      var scroll_top = initial_scroll_top;
      if (new_width <= window.innerWidth) {
        // center container by offsetting its dimension by half of how
        // bigger the window's is
        video_container.style.left = Math.round((window.innerWidth - new_width)/2)+'px';

        scroll_left = 0;
      }
      else {
        video_container.style.left = '0px';

        /* when scaling to 100% or coming from a scaled dimension
         * smaller than the window, center window instead of maintaing
         * the current scroll, otherwise, scroll window from current
         * position */
        if (is_jump || video.dataset.width*start_scale <= window.innerWidth) {
          scroll_left = Math.round((new_width - window.innerWidth)/2);
        }
        else {
          // scroll window by half of how much the video grew
          var width_delta = new_width - video.dataset.width*start_scale;
          scroll_left = initial_scroll_left + Math.round(width_delta/2);
        }
      }

      var new_page_height = get_page_height(new_scale);
      if (new_page_height <= window.innerHeight) {
        // center container by offsetting its dimension by half of how
        // bigger the window's is
        video_container.style.top = Math.round((window.innerHeight - new_page_height)/2)+'px';

        scroll_top = 0;
      }
      else {
        video_container.style.top = '0px';

        /* when scaling to 100% or coming from a scaled dimension
         * smaller than the window, center window instead of maintaing
         * the current scroll, otherwise, scroll window from current
         * position */
        if (is_jump || video.dataset.height*start_scale <= window.innerHeight) {
          scroll_top = Math.round((new_page_height - window.innerHeight)/2);
        }
        else {
          // scroll window by half of how much the video grew
          var height_delta = new_page_height - video.dataset.height*start_scale;
          scroll_top = initial_scroll_top + Math.round(height_delta/2);
        }
      }

      // don't try to scroll to a negative position
      if (scroll_left < 0) { scroll_left = 0; }
      if (scroll_top < 0) { scroll_top = 0; }

      window.scrollTo(scroll_left, scroll_top);

      if (debug) { update_debug_div(); }
    }

    /* keep track of whether the user is performing some other action that
     * involves clicking and dragging, so we can avoid zooming or scolling then */
    var doing_something_else = false;
    video.addEventListener('seeking', function() {
      // ignore the automatic seek event at every loop
      if (this.currentTime > 0) {
        doing_something_else = true;
      }
    });
    video.addEventListener('volumechange', function() {
      doing_something_else = true;
    });

    function scale_on_mouse_drag(event) {
      //starting coordinates
      var start_point = [event.clientX, event.clientY];

      // starting window position
      initial_scroll_left = window.pageXOffset;
      initial_scroll_top = window.pageYOffset;

      var might_be_click = true; // mouse hasn't moved enough, might be a click
      doing_something_else = false;

      function scale_mouse_move_handler(event) {
        /* Scale video according to how farther away on the Y axis from the
         * mousedown position is the mouse now. Down grows the video and up
         * shrinks it. */

        // don't zoom if seeking or changing volume
        if (doing_something_else) { return; }

        var current_point = [event.clientX, event.clientY];
        // don't zoom further if mouse went above the window's top
        if (current_point[1] < 0) {
          current_point[1] = 0;
        }

        var mouse_delta = current_point[1] - start_point[1];

        // arbitrarily chosen amount of pixels the user might accidentally move
        // when actually trying to only click
        var mouse_delta_tolerance = 3;
        if (Math.abs(current_point[0]-start_point[0]) > mouse_delta_tolerance ||
            Math.abs(current_point[1]-start_point[1]) > mouse_delta_tolerance) {
              might_be_click = false;
            }

        // perform zoom
        if (!might_be_click) {

          // scale multiplies the video's dimensions, so we calculate the
          // new scale based on how many pixels we want to add (or
          // subtract) to the video's diagonal
          var video_diagonal = Math.sqrt(Math.pow(video.dataset.width, 2) + Math.pow(video.dataset.height, 2));
          var new_scale_candidate = start_scale + (mouse_delta*mouse_video_delta_multiplier)/video_diagonal;

          // enforce minimum scale (chosen arbitrarily)
          var minimum_dimension = 20;
          if (video.dataset.width*new_scale_candidate >= minimum_dimension &&
              video.dataset.height*new_scale_candidate >= minimum_dimension) {
            new_scale = new_scale_candidate;
          }

          scale_and_position(false);

          event.preventDefault();
          return false;
        }
      }

      function scale_mouse_up_handler() {
        start_scale = new_scale;
        document.removeEventListener('mousemove', scale_mouse_move_handler);
        document.removeEventListener('mouseup', scale_mouse_up_handler);
      }

      function scale_mouse_click_handler(event) {
        // stop click's default action unless the mouse was not moved
        video.removeEventListener('click', scale_mouse_click_handler);
        if ((!might_be_click) && (!doing_something_else)) {
          event.preventDefault();
          return false;
        }
      }

      document.addEventListener('mousemove', scale_mouse_move_handler);
      document.addEventListener('mouseup', scale_mouse_up_handler);
      video.addEventListener('click', scale_mouse_click_handler);
    }

    function scroll_on_mouse_drag(event) {
      //starting coordinates
      var start_point = [event.clientX, event.clientY];

      // starting window position
      initial_scroll_left = window.pageXOffset;
      initial_scroll_top = window.pageYOffset;

      doing_something_else = false;
      var scroll_mouse_delta_multiplier = 2;

      function scroll_mouse_move_handler(event) {
        /* scroll page in the opposite direction of mouse movement (so the user
         * is dragging the video in the direction of mouse movement) */

        // don't scroll if seeking or changing volume
        if (doing_something_else) { return; }

        var current_point = [event.clientX, event.clientY];
        var mouse_delta_x = current_point[0] - start_point[0];
        var mouse_delta_y = current_point[1] - start_point[1];

        // a little tolerance to allow seeking and changing volume before we
        // start moving the page around
        var mouse_delta_tolerance = 3;
        if (Math.abs(current_point[0]-start_point[0]) <= mouse_delta_tolerance &&
            Math.abs(current_point[1]-start_point[1]) <= mouse_delta_tolerance) {
              return;
            }

        var scroll_left = initial_scroll_left - mouse_delta_x*scroll_mouse_delta_multiplier;
        if (scroll_left < 0) {
          scroll_left = 0;
        }
        var scroll_top = initial_scroll_top - mouse_delta_y*scroll_mouse_delta_multiplier;
        if (scroll_top < 0) {
          scroll_top = 0;
        }

        window.scrollTo(scroll_left, scroll_top);

        if (debug) { update_debug_div(); }
      }

      function scroll_mouse_up_handler() {
        document.removeEventListener('mousemove', scroll_mouse_move_handler);
        document.removeEventListener('mouseup', scroll_mouse_up_handler);
      }

      document.addEventListener('mousemove', scroll_mouse_move_handler);
      document.addEventListener('mouseup', scroll_mouse_up_handler);
    }

    video.addEventListener('mousedown', function(event) {
      // left button: scale
      if (event.button === 0) {
        scale_on_mouse_drag(event);
      }
      // middle button: scroll
      else if (event.button === 1) {
        scroll_on_mouse_drag(event);
      }
    });

    var zoom_normal = document.getElementById('zoom_normal');
    if (zoom_normal) {
      zoom_normal.addEventListener('click', function() {
        // back to 100%
        new_scale = start_scale = 1;
        scale_and_position(true);
      });
    }

    function scale_to_fit() {
      //scale to fit horizontally
      new_scale = window.innerWidth/(video.dataset.width);

      //scale to fit vertically
      if (get_page_height(new_scale) > window.innerHeight) {
        /* calculate scale based on whatever height is left after we discount
         * the overhead */
        var available_height = window.innerHeight - get_page_height(0);
        // chosen arbitrarily
        var minimum_video_height = 20;
        if (available_height < minimum_video_height) {
          available_height = minimum_video_height;
        }
        new_scale = available_height/video.dataset.height;
      }

      start_scale = new_scale;
      scale_and_position(true);
    }

    var zoom_to_fit = document.getElementById('zoom_to_fit');
    if (zoom_to_fit) {
      zoom_to_fit.addEventListener('click', function() {
        // fit to window size
        scale_to_fit();
      });
    }

    /* when going full screen, we must reset the scale to 1, otherwise the user
     * will get a video that is either too small or too big for his screen */
    video.addEventListener('dblclick', function() {
        new_scale = start_scale = 1;
        scale_and_position(true);
    });

    /* apply starting values immediately if the video is already loaded (for
     * example, if it is already in cache) or sets event listener to do so
     * once it gets loaded */
    function apply_starting_values() {
      scale_and_position();
      window.addEventListener('resize', function() {
        scale_and_position(true);
      });

      /* video container centers initially because we haven't set width yet
       * (since we don't have the video's width available), but after the
       * initial scale_and_position, we must change its text-align to left */
      video_container.style.textAlign = 'left';

      // fit window on load (only if video is big enough)
      if (video.clientWidth > window.innerWidth || get_page_height(1) > window.innerHeight) {
        scale_to_fit();
      }
    }

    apply_starting_values();

    // setup zoom bar
    function zoom_bar_input_handler() {
      if (zoom_bar && zoom_bar.value !== new_scale*100) {
        start_scale = new_scale = zoom_bar.value/100;
        scale_and_position(true);
      }
    }

    if (zoom_bar) {
      zoom_bar.addEventListener('input', zoom_bar_input_handler);
      zoom_bar.addEventListener('change', zoom_bar_input_handler);
    }
  }

  function create_stylesheet() {
      var sheet, sheet_elem, rules, i;

      sheet_elem = document.createElement('style');
      document.head.appendChild(sheet_elem);
      sheet = sheet_elem.sheet;

      // rules to be inserted
      rules = '.hidden { display: none; }';
      rules += '\n#body { display: inline; margin: 0px; color: #fff; background: '+background_color+'; }\n#body a { color: #BDF; }';
      rules += '\n#video { position: static; margin: 0px; }\n#video_container { overflow: hidden; position: absolute; }';
      rules += '\n#footer, .footer_expander_wrapper { background-color: rgba(0, 0, 0, 0.5); position: fixed; bottom: 0; left: 0; }\n#footer { width: 100%; margin: 0; text-align: center; padding: 2px 0; z-index: 2; }\n.footer_expander_wrapper { display: inline-block; padding: 4px 5px; z-index: 3; }\n.footer_expander_wrapper.hidden { display: none; }\n#footer_expander, #footer_collapser, #rotate_link { cursor: pointer; }\n#rotate_link { line-height: 1em; vertical-align: middle; }\n#footer_collapser { margin: 2px 5px; float: left; }\n.controls { display: inline-block; }\n.control { display: inline-block; margin: 0 0.2em; }\n.control input { vertical-align: middle; }\n#footer button { vertical-align: bottom; }\n#reload_button { float: right; margin: 0 2px; }';
      rules += '\n#debug_div { position: fixed; top: 10px; left: 10px; background: #000; z-index: 3; display: inline; }';

      // insert each rule
      rules = rules.split('\n');
      for (i=0; i<rules.length; i++) {
        sheet.insertRule(rules[i], i);
      }
  }

  function toggle_footer(event) {
    /* hides or unhides footer */
    var footer, expander;

    event.preventDefault();

    footer = document.getElementById('footer');
    expander = document.getElementById('footer_expander');

    if (footer.classList.contains('hidden')) {
      footer.classList.remove('hidden');
      expander.parentNode.classList.add('hidden');
    }
    else {
      footer.classList.add('hidden');
      expander.parentNode.classList.remove('hidden');
    }
  }

  var controls_created = false;
  function create_controls() {
    if (!controls_created) {
      create_stylesheet();

      var body = document.body;
      body.id = 'body';

      var videos = document.getElementsByTagName('video');
      if (videos.length === 0) { return; }

      var video = videos[0];

      // check that this is not actually just an audio track
      if (video.videoWidth === 0 && video.videoHeight === 0) {
        console.log('video track not found; aborting');

        if (loop_audio) {
          video.loop = 'loop';
        }

        controls_created = true;
        return;
      }

      video.id = 'video';
      video.loop = 'loop';
      video.autoplay = 'autoplay';
      video.dataset.width = video.videoWidth;
      video.dataset.height = video.videoHeight;
      video.style.maxWidth = video.videoWidth+'px';
      video.style.maxHeight = video.videoHeight+'px';

      var video_container = document.createElement('div');
      video_container.id = 'video_container';
      video_container.appendChild(video);
      body.appendChild(video_container);
      video.play();

      // create footer
      var tmp_div = document.createElement('div');
      tmp_div.innerHTML = '<div class="footer_expander_wrapper hidden"><a id="footer_expander">&gt;</a></div><div id="footer" class="play_footer fixed"><a id="footer_collapser">&lt;</a><div class="controls" id="controls_div"><div class="control"> speed: <input type="range" id="speed_bar" value="100" min="10" max="200" step="1" /><span id="speed_value">100%</span><button id="speed_normal">reset</button></div><div class="control"> zoom: <input type="range" id="zoom_bar" value="100" min="10" max="200" step="1" /><span id="zoom_value">100%</span><button id="zoom_normal">reset</button><button id="zoom_to_fit">fit</button></div></div><a id="rotate_link">&#8631;</a><button id="reload_button">reload video</button>';
      while (tmp_div.firstChild) {
        body.appendChild(tmp_div.firstChild);
      }

      var debug_div;
      if (debug) {
        debug_div = document.createElement('div');
        debug_div.id = 'debug_div';
        body.appendChild(debug_div);
        debug_div = document.getElementById('debug_div');
      }

      controls_created = true;
    }

    document.getElementById('footer_expander').addEventListener('click', toggle_footer);
    document.getElementById('footer_collapser').addEventListener('click', toggle_footer);

    setup_speed_controls();
    setup_zoom_controls();
  }

  function wait_for_video() {
    var video, videos = document.getElementsByTagName('video');

    if (videos.length === 0) { return; }

    video = videos[0];

    if (video.readyState >= 2) {
      create_controls();
    }
    else {
      video.addEventListener('loadeddata', create_controls);
    }
  }

  function clear_videos() {
    /* make sure all videos are removed (there seems to be a firefox bug where
     * they'll remain playing in the background if you time a video change just
     * as the video loops) */
    var found, video;
    found = document.getElementsByTagName('video');
    while (found.length > 0) {
      video = found[0];
      // pause video
      video.pause();
      // remove sources
      while (video.firstChild) {
        video.removeChild(video.firstChild);
      }
      // remove video
      video.parentNode.removeChild(video);
    }
  }

  function setup() {
    wait_for_video();

    /* make sure to remove video before unloading the page */
    window.addEventListener('unload', clear_videos);
  }

  function check_page_and_run() {
    /* make sure the current document is a video, otherwise don't run at all */
    // document.contentType is only available in firefox
    if (document.contentType !== undefined) {
      // if document is not a video, return immediately
      if (!(/^video\//.test(document.contentType))) { return; }
    }
    else {
      /* for non-firefox, check that the document consists of a single video */
      if (document.body.childNodes.length !== 1) { return; }
      if (document.body.childNodes[0].tagName !== 'VIDEO') { return; }
    }
    /* also check that we're not inside a frame */
    try {
      if (window.self !== window.top) { return; }
    }
    catch (e) { return; }

    setup();
  }

  /* videos don't fire DOMContentLoaded or load like regular pages, so we can
   * only hope the video is already in place by the time we run */
  check_page_and_run();

}());
