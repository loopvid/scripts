// ==UserScript==
// @name          Selca - thumb to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects selca thumbs to original images
// @include       http://selca.kastden.org/thumb/*
// @include       https://selca.kastden.org/thumb/*
// @version       1.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var url = window.location.href;
  // get rid of image so it won't load twice
  var images = document.getElementsByTagName('img');
  if (images.length > 0) {
    images[0].parentNode.removeChild(images[0]);
  }

  // Replace images with original and strip the extension
  var new_url = url.replace('thumb', 'original');
  new_url = new_url.replace(/\.[^/.]+$/, "");

  // make sure we don't loop infinitely
  if (new_url !== url) {
    // redirect
    window.location.replace(new_url);
  }

}());
