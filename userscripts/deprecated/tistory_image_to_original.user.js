// ==UserScript==
// @name          Tistory - image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects tistory images to the original.
// @include       http*://cfile*.uf.tistory.com/image/*
// @include       http*://cfile*.uf.daum.net/image/*
// @include       http*://*.daumcdn.net/thumb/*cfile*.uf.tistory.com*
// @include       http*://*.daumcdn.net/thumb/*cfile*.uf.daum.net*
// @version       2.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  function remove_image() {
    /* get rid of image so it won't load twice */
    var images = document.getElementsByTagName('img');
    if (images.length > 0) {
      images[0].parentNode.removeChild(images[0]);
    }
  }

  function redirect() {
    /* redirect to original image */
    var new_url, match,
        tistory_regex = /cfile(\d+)\.uf\.(tistory\.com|daum\.net).+([0-9A-Z]{22})/;

    // extract the image's server and hash
    match = tistory_regex.exec(window.location.href);
    if (!match) { return; }

    remove_image();

    // build the original image's url
    new_url = 'http://cfile'+match[1]+'.uf.'+match[2]+'/original/'+match[3];

    // make sure we don't loop infinitely
    if (new_url !== window.location.href) {
      // redirect
      window.location.replace(new_url);
    }
  }

  // run immediately
  redirect();

}());
