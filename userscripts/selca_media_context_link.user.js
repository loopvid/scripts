// ==UserScript==
// @name          Selca media context link
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Add context link to post info
// @include       https://selca.kastden.org/*
// @version       1.0.1
// @grant         none
// ==/UserScript==

(function() {
  'use strict';
  var last_href = null,
    delta_days = 7,
    debug_level = 0;

  function format_date(date) {
    /* return date string in the format expected by selca */

    // '%Y-%m-%dT%H:%M:%S.000Z' -> '%Y-%m-%d %H:%M:%S'
    return date.toISOString().replace('T', ' ').replace('.000Z', '');
  }

  function check_for_media_post_page() {
    // don't check again until we navigate to a different url
    if (window.location.href === last_href) { return; }

    if (debug_level >= 1) {
      console.log(last_href + ' -> ' + window.location.href);
    }

    last_href = window.location.href;

    var post_info, context_link, owner_link, post_link, url, date;

    if (window.location.pathname.search('/media/') >= 0) {
      post_info = document.getElementById('cover_media_info');
    }
    else if (window.location.pathname.search('/post/') >= 0) {
      post_info = document.querySelector('.post_page>.post_info');
    }
    else {
      // nothing to do if not a media or post page
      return;
    }

    if (debug_level >= 1) {
      console.log(post_info);
    }

    if (post_info.querySelector('.context_link')) {
      // element already has a context link; nothing else to do
      return;
    }

    // locate elements with information we need to extract
    owner_link = post_info.querySelector('a.user');
    post_link = post_info.querySelector('a.post_link');

    if (!(owner_link && owner_link.href && post_link && post_link.dataset.date)) {
      console.log("Couldn't find owner and post links");
      return;
    }

    // create link element
    context_link = document.createElement('A');
    context_link.classList.add('context_link');

    // build url
    url = owner_link.href;

    date = new Date(post_link.dataset.date);

    if (debug_level >= 1) {
      console.log('url=' + url + ' date=' + date);
    }

    // generate range by shifting the datetime backwards and forwards by the
    // desired delta
    date.setMinutes(date.getMinutes() - delta_days*24*60);
    url += '?min_time='+format_date(date);

    date.setMinutes(date.getMinutes() + 2*delta_days*24*60);
    url += '&max_time='+format_date(date);

    if (debug_level >= 1) {
      console.log(url);
    }

    context_link.href = url;

    context_link.appendChild(document.createTextNode('context'));

    // add link to post info
    owner_link.parentNode.appendChild(context_link);
  }

  function set_up_mutation_observer() {
    var observer;

    /* observe document for changes in the page so we know when we're going
     * into and out of media pages */
    observer = new MutationObserver(check_for_media_post_page);
    observer.observe(document, {childList: true, subtree: true});
  }

  function initial_set_up() {
    // only observe page if there is a grid present
    if (document.getElementById('entries')) {
      set_up_mutation_observer();
    }

    check_for_media_post_page();
  }

  initial_set_up();

}());
